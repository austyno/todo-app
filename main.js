// Modules to control application life and create native browser window
const {app, BrowserWindow,Menu,ipcMain} = require('electron');
const url = require('url');
const path = require('path');
process.env.NODE_ENV = 'Dev';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let addWindow;

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', ()=>{
  const mainMenu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(mainMenu);
  createWindow();
})

const menuTemplate = [
  {
    label : 'file',
    submenu:[
      {
        label: 'Add Items',
        accelerator:'CmdOrCtrl+O',
        click(){
          createAddWindow();
        }
      },
      {
        label:'Clear Todo',
        click(){
          mainWindow.webContents.send('clear-all');
        }
      },
      {
        label:'Clear Completed',
        click(){
          mainWindow.webContents.send('clear-completed');
        }
      },
      {
        role: 'reload'
      }
    ]
  },
  {
    label: ' Dev Tools',
    submenu: [
      {
        label: 'Toggle Devtools',
        click(item,focusedwindow){
          focusedwindow.toggleDevTools();
        }
      }
  ]
  }
];

ipcMain.on('add-item',function(e,item){
 
  mainWindow.webContents.send('add-item',item);
  addWindow.close();
});

if (process.platform === 'darwin') {
  const name = app.getName();
  menuTemplate.unshift({
    label: name,
    submenu: [{
      label: 'Quit',
      accelerator: 'Command+Q',
      click: function () {app.quit() }
    }] 
  })
}

function createAddWindow(){
  addWindow = new BrowserWindow({
    height:200,
    width:300,
    title: 'Add Todo'
  });

  addWindow.loadURL(url.format({
    pathname: path.join(__dirname,'addtodo.html'),
    protocol:'file:',
    slashes: true
  }))

};
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({})

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname,'mainwindow.html'),
    protocol:'file:',
    slashes: true
  }))

  // if(process.env.NODE_ENV !== 'production'){
  //   menuTemplate.push({
  //     label : 'Developer Tools',
  //     submenu:[
  //       {
  //         label : 'Toggle Dev Tools',
  //         accelerator : process.platform == 'darwin'? 'command+I': 'ctrl+I',
  //         click(item,focusedwindow){
  //           focusedwindow.toggleDevTools();
  //         }
  //       }
  //     ]
  //   });
  // }

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
    addWindow = null;
  })
}



// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
